<?php 
  require("appData/define.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo _APP_NAME_ ?></title>
  <base href="<?php echo _BASE_TEMPLATE_ ?>">
  <?php 
    #Librerías CSS
    include("pages/includes/lib/css_lib.php");
    #Librerías JS
    include("pages/includes/lib/js_lib.php");
  ?>
</head>
<?php 
  #Validar si existe sesión
  //include("pages/index.php");
  if(isset($_SESSION[_SESSION_NAME_])){
    include("pages/index.php");
  }else{
    include("pages/login.php");
  }
?>
</html>
