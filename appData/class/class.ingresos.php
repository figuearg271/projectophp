<?php  

class ingresos {

	function __construct() {}
	
	#Inicio de sesión
    function list($params=array()){
        $response = array();
    	$sqlmanager = new sqlmanager();
    	$connection = $sqlmanager->connect(); 
    	if ($connection!=null) {
			$response["status"] = "success";
	    	try {
				$sql = 'SELECT r.id_recibo_agua, CONCAT(p.nombre, " ", p.apellido) as nombre, r.subtotal,r.total,r.interes_mensual,DATE_FORMAT(r.fechaEmitido, "%d/%m/%Y") as fechaEmitido,DATE_FORMAT(r.fechaCancelado, "%d/%m/%Y") as fechaCancelado,DATE_FORMAT(r.fechaVencimiento, "%d/%m/%Y") as fechaVencimiento, r.estado
                        from recibo_agua r 
                        inner join persona p on p.id_contribuyente = r.id_contribuyente where r.estado = 0'; 
				$query = $connection->prepare($sql); 
		    	if ($query->execute()){
		    		$response["object"] = $query->fetchAll(PDO::FETCH_ASSOC);
					$response["total"] = $query->rowCount();
		    	} else {
		            $response = array("status"=>"error", "error"=>"No se pudo ejecutar la consulta a la base de datos");
		        }
			} catch(PDOException $exception) {
		    	$response = array("status"=>"error", "error"=>"Ocurrió el siguiente error: " . $exception->getMessage());
            } finally {
                $sqlmanager->disconnect();
            }
	    } else {
	    	$response = array("status"=>"error", "error"=>"No está conectado al servidor de bases de datos");
	    } 
	    return $response;
    }
	function listAll($params=array()){
        $response = array();
    	$sqlmanager = new sqlmanager();
    	$connection = $sqlmanager->connect(); 
    	if ($connection!=null) {
			$response["status"] = "success";
	    	try {
				$sql = 'SELECT r.id_recibo_agua, CONCAT(p.nombre, " ", p.apellido) as nombre, r.subtotal,r.total,r.interes_mensual,DATE_FORMAT(r.fechaEmitido, "%d/%m/%Y") as fechaEmitido,DATE_FORMAT(r.fechaCancelado, "%d/%m/%Y") as fechaCancelado,DATE_FORMAT(r.fechaVencimiento, "%d/%m/%Y") as fechaVencimiento, r.estado
                        from recibo_agua r 
                        inner join persona p on p.id_contribuyente = r.id_contribuyente  '; 
				$query = $connection->prepare($sql); 
		    	if ($query->execute()){
		    		$response["object"] = $query->fetchAll(PDO::FETCH_ASSOC);
					$response["total"] = $query->rowCount();
		    	} else {
		            $response = array("status"=>"error", "error"=>"No se pudo ejecutar la consulta a la base de datos");
		        }
			} catch(PDOException $exception) {
		    	$response = array("status"=>"error", "error"=>"Ocurrió el siguiente error: " . $exception->getMessage());
            } finally {
                $sqlmanager->disconnect();
            }
	    } else {
	    	$response = array("status"=>"error", "error"=>"No está conectado al servidor de bases de datos");
	    } 
	    return $response;
    }
    function activar($params=array()){
    	$response = array();
    	$sqlmanager = new sqlmanager();
    	$connection = $sqlmanager->connect(); 
    	if (!empty($params)) {
	    	if ($connection!=null) {
				$response["status"] = "success";
		    	try {
		    		$connection->beginTransaction();
					// update tblmenu set nnombre = :nombre w
					$sql = "UPDATE recibo_agua SET estado = :estado where id_recibo_agua = :id_recibo_agua";
					$query = $connection->prepare($sql);
					$query->bindParam(":id_recibo_agua", $params["id_recibo_agua"], PDO::PARAM_INT);
					$query->bindParam(":estado", $params["estado"], PDO::PARAM_INT); 
			    	if ($query->execute()){ 
						$response["total"] = $query->rowCount();
						$connection->commit();
			    	} else {
			            $response = array("status"=>"error", "error"=>"No se pudo ejecutar la consulta a la base de datos");
			        }
				} catch(PDOException $exception) {
					$connection->rollback();
			    	$response = array("status"=>"error", "error"=>"Ocurrió el siguiente error: " . $exception->getMessage());
	            } finally {
	                $sqlmanager->disconnect();
	            }
		    } else {
		    	$response = array("status"=>"error", "error"=>"No está conectado al servidor de bases de datos");
		    }
		} else {
			$response = array("status"=>"error", "error"=>"No está enviando ningún parámetro a la función");
		} 
	    return $response;
    }
    
}

?>