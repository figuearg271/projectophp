<?php  

class contadores {

	function __construct() {}
	
	#Inicio de sesión
    function list($params=array()){
        $response = array();
    	$sqlmanager = new sqlmanager();
    	$connection = $sqlmanager->connect(); 
    	if ($connection!=null) {
			$response["status"] = "success";
	    	try {
				$sql = 'SELECT c.id_contador, date_format(c.puesto_en_servicio, "%d/%m/%Y") as puesto_en_servicio, m.medida, date_format(m.realizada, "%d/%m/%Y") as realizada, concat(p.nombre," ", p.apellido) as nombre, c.estado FROM contador c 
                         inner join medida m on m.id_contador = c.id_contador inner join persona p on p.id_contribuyente = c.id_contribuyente  '; 
				$query = $connection->prepare($sql); 
		    	if ($query->execute()){
		    		$response["object"] = $query->fetchAll(PDO::FETCH_ASSOC);
					$response["total"] = $query->rowCount();
		    	} else {
		            $response = array("status"=>"error", "error"=>"No se pudo ejecutar la consulta a la base de datos");
		        }
			} catch(PDOException $exception) {
		    	$response = array("status"=>"error", "error"=>"Ocurrió el siguiente error: " . $exception->getMessage());
            } finally {
                $sqlmanager->disconnect();
            }
	    } else {
	    	$response = array("status"=>"error", "error"=>"No está conectado al servidor de bases de datos");
	    } 
	    return $response;
    }

	function actlist($params=array()){
        $response = array();
    	$sqlmanager = new sqlmanager();
    	$connection = $sqlmanager->connect(); 
    	if ($connection!=null) {
			$response["status"] = "success";
	    	try {
				$sql = 'SELECT c.id_contador, date_format(c.puesto_en_servicio, "%d/%m/%Y") as puesto_en_servicio, m.medida, date_format(m.realizada, "%d/%m/%Y") as realizada, concat(p.nombre," ", p.apellido) as nombre, c.estado FROM contador c 
                         inner join medida m on m.id_contador = c.id_contador inner join persona p on p.id_contribuyente = c.id_contribuyente where c.estado = 0  '; 
				$query = $connection->prepare($sql); 
		    	if ($query->execute()){
		    		$response["object"] = $query->fetchAll(PDO::FETCH_ASSOC);
					$response["total"] = $query->rowCount();
		    	} else {
		            $response = array("status"=>"error", "error"=>"No se pudo ejecutar la consulta a la base de datos");
		        }
			} catch(PDOException $exception) {
		    	$response = array("status"=>"error", "error"=>"Ocurrió el siguiente error: " . $exception->getMessage());
            } finally {
                $sqlmanager->disconnect();
            }
	    } else {
	    	$response = array("status"=>"error", "error"=>"No está conectado al servidor de bases de datos");
	    } 
	    return $response;
    }

	function activar($params=array()){
    	$response = array();
    	$sqlmanager = new sqlmanager();
    	$connection = $sqlmanager->connect(); 
    	if (!empty($params)) {
	    	if ($connection!=null) {
				$response["status"] = "success";
		    	try {
		    		$connection->beginTransaction();
					// update tblmenu set nnombre = :nombre w
					$sql = "UPDATE contador SET estado = :estado where id_contador = :id_contador";
					$query = $connection->prepare($sql);
					$query->bindParam(":id_contador", $params["id_contador"], PDO::PARAM_INT);
					$query->bindParam(":estado", $params["estado"], PDO::PARAM_INT); 
			    	if ($query->execute()){ 
						$response["total"] = $query->rowCount();
						$connection->commit();
			    	} else {
			            $response = array("status"=>"error", "error"=>"No se pudo ejecutar la consulta a la base de datos");
			        }
				} catch(PDOException $exception) {
					$connection->rollback();
			    	$response = array("status"=>"error", "error"=>"Ocurrió el siguiente error: " . $exception->getMessage());
	            } finally {
	                $sqlmanager->disconnect();
	            }
		    } else {
		    	$response = array("status"=>"error", "error"=>"No está conectado al servidor de bases de datos");
		    }
		} else {
			$response = array("status"=>"error", "error"=>"No está enviando ningún parámetro a la función");
		} 
	    return $response;
    }

	function selectList($params=array()){
    	$response = array();
    	$sqlmanager = new sqlmanager();
    	$connection = $sqlmanager->connect(); 
		if ($connection!=null) {
			$response["status"] = "success";
			try {
				$sql = 'SELECT c.id_contador as id, concat("#", c.id_contador, " - ", p.nombre," ", p.apellido) as text FROM contador c inner join persona p on p.id_contribuyente = c.id_contribuyente where c.estado = 1  '; 
				$query = $connection->prepare($sql); 
				if ($query->execute()){
					$response["object"] = $query->fetchAll(PDO::FETCH_ASSOC);
					$response["total"] = $query->rowCount();
				} else {
					$response = array("status"=>"error", "error"=>"No se pudo ejecutar la consulta a la base de datos");
				}
			} catch(PDOException $exception) {
				$response = array("status"=>"error", "error"=>"Ocurrió el siguiente error: " . $exception->getMessage());
			} finally {
				$sqlmanager->disconnect();
			}
		} else {
			$response = array("status"=>"error", "error"=>"No está conectado al servidor de bases de datos");
		}
	    return $response;
    }
    
}

?>