<?php  

class contribuyente {

	function __construct() {}

	function selectList($params=array()){
    	$response = array();
    	$sqlmanager = new sqlmanager();
    	$connection = $sqlmanager->connect(); 
		if ($connection!=null) {
			$response["status"] = "success";
			try {
				$sql = 'SELECT c.id_contribuyente as id, concat(c.nit, " - ", p.nombre," ", p.apellido) as text FROM contribuyente c INNER JOIN persona p on p.id_contribuyente = c.id_contribuyente'; 
				$query = $connection->prepare($sql); 
				if ($query->execute()){
					$response["object"] = $query->fetchAll(PDO::FETCH_ASSOC);
					$response["total"] = $query->rowCount();
				} else {
					$response = array("status"=>"error", "error"=>"No se pudo ejecutar la consulta a la base de datos");
				}
			} catch(PDOException $exception) {
				$response = array("status"=>"error", "error"=>"Ocurrió el siguiente error: " . $exception->getMessage());
			} finally {
				$sqlmanager->disconnect();
			}
		} else {
			$response = array("status"=>"error", "error"=>"No está conectado al servidor de bases de datos");
		}
	    return $response;
    }
	
    function save($params=array()){
    	$response = array();
    	$sqlmanager = new sqlmanager();
    	$connection = $sqlmanager->connect(); 
    	if (!empty($params)) {
	    	if ($connection!=null) {
				$response["status"] = "success";
		    	try {
		    		$connection->beginTransaction();
					// update tblmenu set nnombre = :nombre w
					$sql = "";
					$query = $connection->prepare($sql);
					$query->bindParam(":medida", $params["medida"], PDO::PARAM_INT);
			    	if ($query->execute()){ 
						$idmedida=intval($connection->lastInsertId());
						$response["insertId"] = $idmedida;
						$connection->commit();
			    	} else {
			            $response = array("status"=>"error", "error"=>"No se pudo ejecutar la consulta a la base de datos");
			        }
				} catch(PDOException $exception) {
					$connection->rollback();
			    	$response = array("status"=>"error", "error"=>"Ocurrió el siguiente error: " . $exception->getMessage());
	            } finally {
	                $sqlmanager->disconnect();
	            }
		    } else {
		    	$response = array("status"=>"error", "error"=>"No está conectado al servidor de bases de datos");
		    }
		} else {
			$response = array("status"=>"error", "error"=>"No está enviando ningún parámetro a la función");
		} 
	    return $response;
    }
    
}

?>