<?php  

class recibo_agua {

	function __construct() {}
	
	#Inicio de sesión
    function dataReport($params=array()){
        $response = array();
    	$sqlmanager = new sqlmanager();
    	$connection = $sqlmanager->connect(); 
    	if ($connection!=null) {
			$response["status"] = "success";
	    	try {
				$sql = 'SELECT CONCAT(p.nombre, " ", p.apellido) AS nombre, DATE_FORMAT(r.fechaEmitido, "%d/%m/%Y") AS fechaEmitido, DATE_FORMAT(r.fechaVencimiento, "%d/%m/%Y") AS fechaVencimiento, DATE_FORMAT(r.fechaCancelado, "%d/%m/%Y") AS fechaCancelado, r.interes_mensual, r.subtotal, r.total FROM recibo_agua r INNER JOIN persona p ON p.id_contribuyente = r.id_contribuyente WHERE r.fechaEmitido BETWEEN :fecha_inicio AND :fecha_fin'; 
				$query = $connection->prepare($sql);
				$query->bindParam(":fecha_inicio", $params["fecha_inicio"], PDO::PARAM_STR);
                $query->bindParam(":fecha_fin", $params["fecha_fin"], PDO::PARAM_STR);
		    	if ($query->execute()){
		    		$response["object"] = $query->fetchAll(PDO::FETCH_ASSOC);
					$response["total"] = $query->rowCount();
		    	} else {
		            $response = array("status"=>"error", "error"=>"No se pudo ejecutar la consulta a la base de datos");
		        }
			} catch(PDOException $exception) {
		    	$response = array("status"=>"error", "error"=>"Ocurrió el siguiente error: " . $exception->getMessage() . "");
            } finally {
                $sqlmanager->disconnect();
            }
	    } else {
	    	$response = array("status"=>"error", "error"=>"No está conectado al servidor de bases de datos");
	    } 
	    return $response;
    }

    function dataReportVencidos($params=array()){
        $response = array();
    	$sqlmanager = new sqlmanager();
    	$connection = $sqlmanager->connect(); 
    	if ($connection!=null) {
			$response["status"] = "success";
	    	try {
				$sql = 'SELECT r.id_recibo_agua, CONCAT(p.nombre, " ", p.apellido) as nombre, r.subtotal,r.total,r.interes_mensual,DATE_FORMAT(r.fechaEmitido, "%d/%m/%Y") as fechaEmitido,DATE_FORMAT(r.fechaCancelado, "%d/%m/%Y") as fechaCancelado,DATE_FORMAT(r.fechaVencimiento, "%d/%m/%Y") as fechaVencimiento, r.estado from recibo_agua r inner join persona p on p.id_contribuyente = r.id_contribuyente  where date_format(r.fechaVencimiento, "%Y/%m/%d") <= DATE(NOW())'; 
				$query = $connection->prepare($sql);
		    	if ($query->execute()){
		    		$response["object"] = $query->fetchAll(PDO::FETCH_ASSOC);
					$response["total"] = $query->rowCount();
		    	} else {
		            $response = array("status"=>"error", "error"=>"No se pudo ejecutar la consulta a la base de datos");
		        }
			} catch(PDOException $exception) {
		    	$response = array("status"=>"error", "error"=>"Ocurrió el siguiente error: " . $exception->getMessage() . "");
            } finally {
                $sqlmanager->disconnect();
            }
	    } else {
	    	$response = array("status"=>"error", "error"=>"No está conectado al servidor de bases de datos");
	    } 
	    return $response;
    }

	function save($params=array()){
    	$response = array();
    	$sqlmanager = new sqlmanager();
    	$connection = $sqlmanager->connect(); 
    	if (!empty($params)) {
	    	if ($connection!=null) {
				$response["status"] = "success";
		    	try {
		    		$connection->beginTransaction();
					// update tblmenu set nnombre = :nombre w
					$sql = 'INSERT INTO recibo_agua(id_contribuyente, subtotal, total, interes_mensual, fechaEmitido, fechaCancelado, fechaVencimiento, estado) VALUES(:id_contribuyente, :subtotal, :total, :interes_mensual, :fechaEmitido, NULL, ADDDATE(:fechaVencimiento, INTERVAL 30 DAY), 1)';
					$query = $connection->prepare($sql);
					$query->bindParam(":id_contribuyente", $params["id_contribuyente"], PDO::PARAM_INT);
                    $query->bindParam(":subtotal", $params["subtotal"], PDO::PARAM_STR); 
					$query->bindParam(":total", $params["total"], PDO::PARAM_STR); 
					$query->bindParam(":interes_mensual", $params["interes_mensual"], PDO::PARAM_STR); 
                    $query->bindParam(":fechaEmitido", $params["fechaEmitido"], PDO::PARAM_STR);
					$query->bindParam(":fechaVencimiento", $params["fechaVencimiento"], PDO::PARAM_STR);
			    	if ($query->execute()){ 
						$idmedida=intval($connection->lastInsertId());
						$response["insertId"] = $idmedida;
						$connection->commit();
			    	} else {
			            $response = array("status"=>"error", "error"=>"No se pudo ejecutar la consulta a la base de datos");
			        }
				} catch(PDOException $exception) {
					$connection->rollback();
			    	$response = array("status"=>"error", "error"=>"Ocurrió el siguiente error: " . $exception->getMessage());
	            } finally {
	                $sqlmanager->disconnect();
	            }
		    } else {
		    	$response = array("status"=>"error", "error"=>"No está conectado al servidor de bases de datos");
		    }
		} else {
			$response = array("status"=>"error", "error"=>"No está enviando ningún parámetro a la función");
		} 
	    return $response;
    }
}

?>