<?php  

class usuario {

	function __construct() {}
	
	#Inicio de sesión
    function login($params=array()){
        $response = array();
    	$sqlmanager = new sqlmanager();
    	$connection = $sqlmanager->connect(); 
    	if ($connection!=null) {
			$response["status"] = "success";
	    	try {
				$sql = 'SELECT u.id_usuario, u.usuario, (u.clave=:password) pass_ok, u.id_rol from usuario u where u.usuario=:usuario and u.estado = 1'; 
				$query = $connection->prepare($sql);
				$query->bindParam(":usuario", $params["usuario"], PDO::PARAM_STR);
                $query->bindParam(":password", $params["password"], PDO::PARAM_STR);
		    	if ($query->execute()){
		    		$response["object"] = $query->fetchAll(PDO::FETCH_ASSOC);
					$response["total"] = $query->rowCount();
		    	} else {
		            $response = array("status"=>"error", "error"=>"No se pudo ejecutar la consulta a la base de datos");
		        }
			} catch(PDOException $exception) {
		    	$response = array("status"=>"error", "error"=>"Ocurrió el siguiente error: " . $exception->getMessage());
            } finally {
                $sqlmanager->disconnect();
            }
	    } else {
	    	$response = array("status"=>"error", "error"=>"No está conectado al servidor de bases de datos");
	    } 
	    return $response;
    }
    
}

?>