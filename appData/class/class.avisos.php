<?php  

class avisos {

	function __construct() {}
	
	#Avisos
    function avisos($params=array()){ 
        $response = array();
    	$sqlmanager = new sqlmanager();
    	$connection = $sqlmanager->connect(); 
    	if ($connection!=null) {
			$response["status"] = "success";
	    	try {
				$sql = ' SELECT r.id_recibo_agua, CONCAT(p.nombre, " ", p.apellido) as nombre, r.subtotal,r.total,r.interes_mensual,DATE_FORMAT(r.fechaEmitido, "%d/%m/%Y") as fechaEmitido,DATE_FORMAT(r.fechaCancelado, "%d/%m/%Y") as fechaCancelado,DATE_FORMAT(r.fechaVencimiento, "%d/%m/%Y") as fechaVencimiento, r.estado from recibo_agua r inner join persona p on p.id_contribuyente = r.id_contribuyente  where date_format(r.fechaVencimiento, "%Y/%m/%d") <= DATE(NOW())'; 
				$query = $connection->prepare($sql);  
		    	if ($query->execute()){
		    		$response["object"] = $query->fetchAll(PDO::FETCH_ASSOC);
					$response["total"] = $query->rowCount();
		    	} else {
		            $response = array("status"=>"error", "error"=>"No se pudo ejecutar la consulta a la base de datos");
		        }
			} catch(PDOException $exception) {
		    	$response = array("status"=>"error", "error"=>"Ocurrió el siguiente error: " . $exception->getMessage());
            } finally {
                $sqlmanager->disconnect();
            }
	    } else {
	    	$response = array("status"=>"error", "error"=>"No está conectado al servidor de bases de datos");
	    } 
	    return $response;
    }
 
    
}

?>