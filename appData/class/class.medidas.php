<?php  

class medidas {

	function __construct() {}
	
	#Inicio de sesión
    function list($params=array()){
        $response = array();
    	$sqlmanager = new sqlmanager();
    	$connection = $sqlmanager->connect(); 
    	if ($connection!=null) {
			$response["status"] = "success";
	    	try {
				$sql = 'SELECT c.id_contador, m.medida, DATE_FORMAT(m.realizada, "%d/%m/%Y") as realizada, concat(p.nombre," ", p.apellido) as nombre,  m.estado FROM medida m inner join contador c on c.id_contador = m.id_contador inner join contribuyente co ON c.id_contribuyente=co.id_contribuyente inner join persona p on co.id_contribuyente=p.id_contribuyente order by c.id_contador'; 
				$query = $connection->prepare($sql); 
		    	if ($query->execute()){
		    		$response["object"] = $query->fetchAll(PDO::FETCH_ASSOC);
					$response["total"] = $query->rowCount();
		    	} else {
		            $response = array("status"=>"error", "error"=>"No se pudo ejecutar la consulta a la base de datos");
		        }
			} catch(PDOException $exception) {
		    	$response = array("status"=>"error", "error"=>"Ocurrió el siguiente error: " . $exception->getMessage());
            } finally {
                $sqlmanager->disconnect();
            }
	    } else {
	    	$response = array("status"=>"error", "error"=>"No está conectado al servidor de bases de datos");
	    } 
	    return $response;
    }
    function save($params=array()){
    	$response = array();
    	$sqlmanager = new sqlmanager();
    	$connection = $sqlmanager->connect(); 
    	if (!empty($params)) {
	    	if ($connection!=null) {
				$response["status"] = "success";
		    	try {
		    		$connection->beginTransaction();
					// update tblmenu set nnombre = :nombre w
					$sql = "INSERT INTO medida (medida, realizada, id_contador, estado) VALUES (:medida, :realizada, :id_contador, 1)";
					$query = $connection->prepare($sql);
					$query->bindParam(":medida", $params["medida"], PDO::PARAM_INT);
                    $query->bindParam(":realizada", $params["realizada"], PDO::PARAM_STR); 
                    $query->bindParam(":id_contador", $params["id_contador"], PDO::PARAM_INT);
			    	if ($query->execute()){ 
						$idmedida=intval($connection->lastInsertId());
						$response["insertId"] = $idmedida;
						$connection->commit();
			    	} else {
			            $response = array("status"=>"error", "error"=>"No se pudo ejecutar la consulta a la base de datos");
			        }
				} catch(PDOException $exception) {
					$connection->rollback();
			    	$response = array("status"=>"error", "error"=>"Ocurrió el siguiente error: " . $exception->getMessage());
	            } finally {
	                $sqlmanager->disconnect();
	            }
		    } else {
		    	$response = array("status"=>"error", "error"=>"No está conectado al servidor de bases de datos");
		    }
		} else {
			$response = array("status"=>"error", "error"=>"No está enviando ningún parámetro a la función");
		} 
	    return $response;
    }
    
}

?>