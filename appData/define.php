<?php

@ob_start();
@session_start();
#Excepción de errores internos
#E_ALL=ESTRICTO; E_ERROR=FLEXIBLE;
error_reporting(E_ERROR);
#Dirección del servidor
define("_SERVER_", "localhost");
#Protocolo de dirección (http/https)
define("_PROTOCOL_", "http");
#Nombre de la carpeta del proyecto
define("_PROYECT_NAME_", "/projectophp/");
#Dirección URL de proyecto
define("_BASE_TEMPLATE_", _PROTOCOL_."://"._SERVER_._PROYECT_NAME_);
#Nombre de la sesión del proyecto
define("_SESSION_NAME_", "usuario");
#Nombre del sistema
define("_APP_NAME_", "Sistema de catastro");
#Versión de sistema
define("_APP_VERSION_", "1.0.0");
#ruta de sistema
define("_SYSTEM_DIR_", "C:/wamp64/www"._PROYECT_NAME_);
?>