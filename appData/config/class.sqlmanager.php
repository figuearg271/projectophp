<?php 

@ob_start();
@session_start();

class sqlmanager {
    
    private $port = "3306";
    private $hostname = "localhost";
    private $database = "catastro";
    private $username = "root";
    private $password = "";

    public $connection;

    public function connect(){
        $this->connection = null;
        try {
            //$this->connection = new PDO("sqlsrv:Server=" . $this->hostname . "; Database=" . $this->database, $this->username, $this->password);
            $this->connection = new PDO("mysql:host=" . $this->hostname . "; port=" . $this->port . "; dbname=" . $this->database. "; charset=utf8", $this->username, $this->password);
        } catch(PDOException $exception) {
            //echo "No se pudo conectar la base de datos: " . $exception->getMessage();
        }
        return $this->connection;
    }

    public function disconnect() {
      	$this->connection = null;
   	}
}  

?>