<?php
    require_once "../../define.php";
	include("../../config/class.sqlmanager.php");
	include("../../class/class.usuario.php");

	$usuario = new usuario();

	$params = $_REQUEST;
	$response = array();

	$not_exist = array();
	$param_list = array("usuario", "password");
	foreach ($param_list as $param) {
		if (!(isset($params[$param]))) {
			array_push($not_exist, $param);
		}
	}

	try{
		if (empty($not_exist)) {
			$response = $usuario->login($params);
			if ($response["status"]=="success") {
				if ($response["total"] > 0) {
					$data=$response['object'][0];
					if($data['pass_ok']=="1"){
						#Crear sesion
						$_SESSION[_SESSION_NAME_]=$response['object'][0];
						$response=array("status"=>"success");
					}else{
						$response=array("status"=>"error", "error"=>'La contraseña es incorrecta');
					}
				} else {
					$response = array("status"=>"error", "error" => "El usuario no existe");
				}
			}
		} else {
			$response = array("status"=>"error", "error" => "No se pudo obtener los datos del usuario. Los datos están enviados están incompletos");
		}
	}catch(Exception $e){
		$response=array('status'=>'success', 'error'=>$e->getMessage());
	}
	
	echo json_encode($response);
?>