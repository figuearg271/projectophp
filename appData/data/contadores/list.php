<?php
    require_once "../../define.php";
	include("../../config/class.sqlmanager.php");
	include("../../class/class.contadores.php");

	$contadores = new contadores();

	$params = $_REQUEST;
	$response = array();

	$not_exist = array();
	$param_list = array();
	foreach ($param_list as $param) {
		if (!(isset($params[$param]))) {
			array_push($not_exist, $param);
		}
	}

	try{
		if (empty($not_exist)) {
			$response = $contadores->list($params);
			if ($response["status"]=="error") {
				$response = array("status"=>"success", "object" => array(),"total"=>0,"error"=>$response["error"]);
			}
		} else {
			$response = array("status"=>"error", "error" => "No se pudo obtener los datos del ingresos. Los datos están enviados están incompletos");
		}
	}catch(Exception $e){
		$response=array('status'=>'success', 'error'=>$e->getMessage());
	}
	
	echo json_encode($response);
?>