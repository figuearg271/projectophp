<?php
    require_once "../../define.php";
	include("../../config/class.sqlmanager.php");
	include("../../class/class.recibo_agua.php");
	require_once("../../../plugins/dompdf/dompdf_config.inc.php");

	$recibo_agua = new recibo_agua();

	$params = $_REQUEST;
	$response = array();

	$not_exist = array();
	$param_list = array();
	foreach ($param_list as $param) {
		if (!(isset($params[$param]))) {
			array_push($not_exist, $param);
		}
	}

	try{
		if (empty($not_exist)) {
			$response = $recibo_agua->dataReportVencidos($params);
			if ($response["status"]=="error") {
				$response = array("status"=>"success", "error"=>$response["error"]);
			}else{
				
				$html = '';
				$tbody = '';
				if ($response['total']>0) {
					foreach ($response["object"] as $key => $value) {
						$tbody.='<tr><td>' . $value['nombre'] . '</td><td>' . $value['fechaEmitido'] . '</td><td>' . $value['fechaVencimiento'] . '</td><td>' . $value['fechaCancelado'] . '</td><td>' . $value['interes_mensual'] . '</td><td>' . $value['subtotal'] . '</td><td>' . $value['total'] . '</td></tr>';
					}
				}
				$html.='<table class="table-bordered"><thead><tr><th>NOMBRE</th><th>FECHA EMISIÓN</th><th>FECHA VENCIMIENTO</th><th>FECHA CANCELADO</th><th>INTERES MENSUAL</th><th>SUBTOTAL</th><th>TOTAL</th></tr></thead><tbody>' . $tbody . '</tbody></table>';
				$encabezado = $html; 

				$html='';
				$contenido=$encabezado;

				$html.='<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/></head><body style="padding-top: 110px; padding-bottom: 50px;"><style>.table-bordered {width: 100%;border: 1px solid #000;border-collapse: collapse;}.table-bordered tr, .table-bordered td, .table-bordered th {border: 1px solid #000;font-size: 7pt !important; text-align: left; }</style>[contenido]</body></html>';
				$html=str_replace('[contenido]', $contenido, $html);

				//image(ruta de imagen, eje x, eje y, ancho, alto);
				//line(eje x, eje y, eje x2, eje y2, color, tamaño);
				//page_text(eje x, eje y, "texto de encabezado", $tipo de fuente, tamaño de fuente, color (format RGB, array(x/255, y/255, z/255)));
				$dompdf = new DOMPDF();
				$dompdf->load_html(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
				$dompdf->set_paper('letter', 'landscape');
				$font = Font_Metrics::get_font('helvetica', 'bold');
				$dompdf->render();
				//Titulo de PDF
				$canvas = $dompdf->get_canvas();
				$header = $canvas->open_object();
				$canvas->page_script('     
					$font = Font_Metrics::get_font("helvetica", "bold");
					$pdf->text(350, 40, "REPORTE DE AVISOS", $font, 18, array(.145, .125, .223));
					$pdf->text(328, 70, "RECIBOS DE AGUA VENCIDOS", $font, 14, array(0, 0, 0));
				');
				$canvas->close_object();
				$canvas->add_object($header, 'all');
				//Pie de PDF
				$footer = $canvas->open_object();
				$canvas->page_text(305,550, 'Pagina: {PAGE_NUM} de {PAGE_COUNT} - - - -  Impreso el '.date('d/m/Y').' a las '.date('H:i:s'), $font, 8, array(0,0,0));
				$canvas->close_object();
				$canvas->add_object($footer, 'all');
				
				if (@file_put_contents('../../../../'._PROYECT_NAME_.'dist/pdf/file.pdf', $dompdf->output())){
					$response=array('status'=>"success", 'url'=>_BASE_TEMPLATE_."dist/pdf/file.pdf?random=".md5(date('d-m-Y H:i:s')), 'html'=>"<iframe src='"._BASE_TEMPLATE_."dist/pdf/file.pdf?random=".md5(date('d-m-Y H:i:s'))."' width='100%' height='100%'></iframe>");
				} else {
					$response=array('status'=>"error", 'error'=>'No se pudo generar el PDF');
				}

			}
		} else {
			$response = array("status"=>"error", "error" => "No se pudo obtener los datos del ingreso. Los datos están enviados están incompletos");
		}
	}catch(Exception $e){
		$response=array('status'=>'success', 'error'=>$e->getMessage());
	}
	
	echo json_encode($response);
?>