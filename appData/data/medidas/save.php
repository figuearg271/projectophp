<?php
    require_once "../../define.php";
	include("../../config/class.sqlmanager.php");
	include("../../class/class.medidas.php");

	$medidas = new medidas();

	$params = $_REQUEST;
	$response = array();

	$not_exist = array();
	$param_list = array("medida", "realizada", "id_contador");
	foreach ($param_list as $param) {
		if (!(isset($params[$param]))) {
			array_push($not_exist, $param);
		}
	}

	try{
		if (empty($not_exist)) {
			$response = $medidas->save($params); 
            if ($response["status"]=="error") {
                $response = array("status"=>"error", "error" => "La medida no pudo ser ingresada");
            }
		} else {
			$response = array("status"=>"error", "error" => "No se pudo obtener los datos del ingresos. Los datos están enviados están incompletos");
		}
	}catch(Exception $e){
		$response=array('status'=>'success', 'error'=>$e->getMessage());
	}
	
	echo json_encode($response);
?>