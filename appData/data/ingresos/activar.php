<?php
    require_once "../../define.php";
	include("../../config/class.sqlmanager.php");
	include("../../class/class.ingresos.php");

	$ingresos = new ingresos();

	$params = $_REQUEST;
	$response = array();

	$not_exist = array();
	$param_list = array("id_recibo_agua", "estado");
	foreach ($param_list as $param) {
		if (!(isset($params[$param]))) {
			array_push($not_exist, $param);
		}
	}

	try{
		if (empty($not_exist)) {
			$response = $ingresos->activar($params); 
		} else {
			$response = array("status"=>"error", "error" => "No se pudo obtener los datos del ingresos. Los datos están enviados están incompletos");
		}
	}catch(Exception $e){
		$response=array('status'=>'success', 'error'=>$e->getMessage());
	}
	
	echo json_encode($response);
?>