<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
      <div class="row vh-100 justify-content-around align-items-center" style="margin:auto;">
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
            <a href="javascript:void(0)" class="h5"><b><?php echo _APP_NAME_ ?></b></a>
            </div>
            <div class="card-body">
                <p class="login-box-msg">Debe iniciar sesión</p>
                <form onsubmit="return false" id="frmLogin">
                    <div class="input-group mb-3">
                    <input type="text" name="usuario" class="form-control" placeholder="Usuario">
                    <div class="input-group-append">
                        <div class="input-group-text">
                        <span class="fas fa-user"></span>
                        </div>
                    </div>
                    </div>
                    <div class="input-group mb-3">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    </div>
                    <div class="col-12">
                        <button type="submit" id="btnLogin" form="frmLogin" class="btn btn-primary float-right">Iniciar sesión</button>
                    </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
  </div>
</div>

<!--Llamar archivo JS-->
<script type="text/javascript" src="pages/controller/login.js"></script>