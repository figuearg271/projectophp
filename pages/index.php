<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <!-- Navbar -->
    <?php 
      include("includes/content/header.php");
    ?>
    <!-- Menu izquierda -->
    <?php include("includes/content/menuLeft.php"); ?>
    <!-- Contenido principal -->
    <div class="content-wrapper">
        <?php 
            #Modulos por defecto
            define("_DEFAULT_MODULE_", "view/blank.php"); 
            define("_NOT_FOUND_MODULE_", "view/404.php");
            define("_ERROR_MODULE_", "view/500.php");
            #Verificar si se envían módulo
            $page=(isset($_GET['page'])) ? $_GET['page'] : "";
            #Definición de módulos
            $systemModule=array(
                array( 'page'=>'contadores', 'url'=>'view/listaContadores.php'),
                array( 'page'=>'arecibos', 'url'=>'view/activarRecibos.php'),
                array( 'page'=>'movimientos', 'url'=>'view/movimientos.php'),
                array( 'page'=>'acontadores', 'url'=>'view/activarContadores.php'),
                array( 'page'=>'ragua', 'url'=>'view/registroAgua.php'),
                array( 'page'=>'reporteI', 'url'=>'view/reporteIngresos.php'),
                array( 'page'=>'reporteAviso', 'url'=>'view/reporteAvisoVencimiento.php'),
                
            );
            #Buscar módulos definidos
            switch($page){ 
                case "inicio":
                    $page = _DEFAULT_MODULE_;
                break;
                case "404":
                    $page=_NOT_FOUND_MODULE_;
                break;
                case "500":
                    $page=_ERROR_MODULE_;
                break;
                default:
                    #buscar el modulo creado
                    if($page==""){
                        $page=_DEFAULT_MODULE_;
                    }else{
                        $flag=false;
                        foreach($systemModule as $key=>$value){
                            if($page==$value['page']){
                                $page=$value['url'];
                                $flag=true;
                                break;
                            }
                        }
                        if(!$flag) $page=_NOT_FOUND_MODULE_; 
                        if (!file_exists(_SYSTEM_DIR_."pages/".$page)) $page=_ERROR_MODULE_;
                    }

                break;
            }
            #Llamar pantalla
            include($page);
        ?>
    </div>
    <?php 
      #Pie de página
      include('includes/content/footer.php'); 
    ?>
  </div>
  <script type="text/javascript" src="pages/controller/logout.js"></script>
  <?php if($_SESSION[_SESSION_NAME_]['id_rol']==1 || $_SESSION[_SESSION_NAME_]['id_rol'] == 2):?>
    <script type="text/javascript" src="pages/controller/avisos.js"></script>
  <?php endif;?>
  <script type="text/javascript">
    $.widget.bridge('uibutton', $.ui.button)
  </script>
</body>