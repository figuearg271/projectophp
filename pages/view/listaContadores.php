<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Lista de contadores</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="javascript:void(0)">Control de agua</a></li>
          <li class="breadcrumb-item active"><a href="javascript:void(0)">Lista de contadores</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>

<section class="content">
  <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Contadores</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="contadores" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th># Contador</th>
                <th>Fecha de servicio</th>
                <th>Medida</th>
                <th>Fecha Realizada</th>
                <th>Cliente</th> 
                <th>Estado</th>     
              </tr>
              </thead>
              <tbody> 
              </tbody> 
            </table>
          </div> 
        </div>
      </div>
  </div>
</section>
<!-- DataTable -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!--Archivo Js-->
<script type="text/javascript" src="pages/controller/listaContadores.js"></script>