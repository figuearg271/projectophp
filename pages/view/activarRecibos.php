

<div class="card">
  <div class="card-header">
    <h3 class="card-title">Recibos</h3>
  </div>
  <div class="card-body">
    <table id="recibos" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>Nombre</th>  
        <th>Fecha Emitido</th>
        <th>Fecha Cancelado</th>
        <th>Fecha Vencimiento</th> 
        <th>Total</th>
        <th>Estado</th>
        <th>Acciones</th> 
      </tr>
      </thead>
      <tbody> 
      </tbody> 
    </table>
  </div> 
</div>
<!-- DataTable -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!--Archivo Js-->
<script type="text/javascript" src="pages/controller/activarRecibos.js"></script>