<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Reporte general de avisos</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="javascript:void(0)">Avisos</a></li>
          <li class="breadcrumb-item active"><a href="javascript:void(0)">Reporte general de avisos</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Contenido -->
<section class="content">
  <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Vencidos hasta hoy</h3><br>
          </div>
          <div class="card-body" style="height:700px;" id="reporteI">
            
          </div> 
        </div>
      </div>
  </div>
</section>
 <script type="text/javascript" src="pages/controller/reporteAvisoVencimiento.js"></script>
