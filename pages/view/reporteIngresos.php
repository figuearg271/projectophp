<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Reporte de ingresos</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="javascript:void(0)">Ingresos diarios</a></li>
          <li class="breadcrumb-item active"><a href="javascript:void(0)">Reporte de ingresos</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Contenido -->
<section class="content">
  <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Reporte</h3><br>
          </div> 
          <div class="card-body">
            <div class="form-group">
              <label>Fecha Inicio:</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                    </div>
                    <input type="date" id="finicio" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                </div><br>
                <label>Fecha Fin:</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                    </div>
                    <input type="date" id="ffin" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                </div>  <br>
                      <button type="button" class="btn btn-block btn-danger" onclick="getReporte()">Generar PDF</button>
                  
             </div>
          </div>
          <div class="card-body" id="reporteI">
            
          </div> 
        </div>
      </div>
  </div>
</section>
 <script type="text/javascript" src="pages/controller/reporteIngresos.js"></script>
