<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Registro de medidas de agua</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="javascript:void(0)">Control de agua</a></li>
          <li class="breadcrumb-item active"><a href="javascript:void(0)">Registro de medidas de agua</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Contenido -->
<section class="content">
  <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Contadores</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-info btn-sm" id="btnModalMedida">Agregar medida</button>
            </div>
          </div>
          <div class="card-body">
            <div class="responsive">
              <table id="ragua" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th># Contador</th> 
                  <th>Contribuyente</th>
                  <th>Medida</th>
                  <th>Fecha Realizada</th> 
                  <th>Estado</th>    
                </tr>
                </thead>
              </table>
            </div>
          </div> 
        </div>
      </div>
  </div>
</section>

<!-- Modal para ingreso de medida -->
<div class="modal fade" id="nuevaMedidaModal" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalToggleLabel2">Nueva medida</h5>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" aria-label="Close"> x </button>
      </div>
      <div class="modal-body">
        <form id="frmMedida" onsubmit="return false"> 
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Medida:</label>
                  <div class="input-group">
                      <input type="number" class="form-control" name="medida" placeholder="Escriba la medida">
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Fecha realizada:</label>
                  <div class="input-group">
                      <input type="date" class="form-control" name="realizada" placeholde="Escriba la fecha">
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Contribuyente :</label>
                  <div class="input-group">
                      <select name="id_contador" style="width:100% !important;" class="form-control" id="contadorSelect"></select>
                  </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary float-left" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-success" id="btnGuardarMedida" form="frmMedida">Guardar</button>
      </div>
    </div>
  </div>
</div>
<!-- DataTable -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!--Archivo Js-->
<script type="text/javascript" src="pages/controller/registroAgua.js"></script>