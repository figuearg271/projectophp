<!-- Titulo y migas de pan -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Error 500</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Error 500</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>

<!-- Contenido -->
<section class="content">
  <div class="error-page">
    <h2 class="headline text-danger">500</h2>
    <div class="error-content">
      <h3><i class="fas fa-exclamation-triangle text-danger"></i> Ups! Algo falló.</h3>
      <p>
        Estamos trabajando en resolverlo lo más pronto posible
        Mientras tanto, puede <a href="?page=inicio">Regresar a página principal</a>.
      </p>
    </div>
  </div>
</section>