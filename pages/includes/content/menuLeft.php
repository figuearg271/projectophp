 <?php 
 $page=(isset($_GET['page'])) ? $_GET['page'] : "";
 ?>
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="#" class="brand-link">
        <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light"><?php echo "APP" ?></span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                <a href="#" class="d-block"><?=$_SESSION[_SESSION_NAME_]['usuario']?></a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                    <li class="nav-header">MENU</li> 
                    <?php if($_SESSION[_SESSION_NAME_]['id_rol']==1 || $_SESSION[_SESSION_NAME_]['id_rol'] == 3):?>
                        <li class="nav-item <?php if($page=="arecibos" || $page=="movimientos" || $page=="reporteI" ): echo "menu-open";  endif;?>">
                            <a href="#" class="nav-link <?php if($page=="arecibos" || $page=="movimientos" || $page=="reporteI"): echo "active";  endif;?>">
                            <i class="far fa-id-badge"></i>
                            <p>
                                Ingresos Diarios
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview" >
                                <li class="nav-item ">
                                    <a href="?page=arecibos" class="nav-link <?php if($page=="arecibos"   ): echo "active";  endif;?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Activar recibos anulados</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="?page=movimientos" class="nav-link <?php if(  $page=="movimientos" ): echo "active";  endif;?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Movimientos</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="?page=reporteI" class="nav-link <?php if(  $page=="reporteI" ): echo "active";  endif;?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Reporte de Ingresos</p>
                                    </a>
                                </li> 
                            </ul>
                        </li>
                    <?php endif;?>
                    <?php if($_SESSION[_SESSION_NAME_]['id_rol']==1 || $_SESSION[_SESSION_NAME_]['id_rol'] == 2):?>
                        <li class="nav-item <?php if($page=="contadores" || $page=="acontadores" || $page=="ragua"   ): echo "menu-open";  endif;?>">
                            <a href="#" class="nav-link <?php if($page=="contadores" || $page=="acontadores" || $page=="ragua"): echo "active";  endif;?>">
                            <i class="fas fa-hand-holding-water"></i>
                            <p>
                                Control de Agua
                                <i class="fas fa-angle-left right"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="?page=acontadores" class="nav-link <?php if(  $page=="acontadores" ): echo "active";  endif;?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Activar contadores</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="?page=ragua" class="nav-link <?php if(  $page=="ragua" ): echo "active";  endif;?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Registro de medidas de agua</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="?page=contadores" class="nav-link <?php if(  $page=="contadores" ): echo "active";  endif;?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Lista de contadores</p>
                                    </a>
                                </li> 
                            </ul>
                        </li>
                    <?php endif;?>
                    <li class="nav-item <?php if($page=="reporteAviso"    ): echo "menu-open";  endif;?>">
                        <a href="#" class="nav-link <?php if($page=="reporteAviso"    ): echo "active";  endif;?>">
                        <i class="fas fa-bullhorn"></i>
                        <p>
                            Avisos
                            <i class="fas fa-angle-left right"></i>
                        </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="?page=reporteAviso" class="nav-link <?php if($page=="reporteAviso"    ): echo "active";  endif;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>R. general de avisos</p>
                                </a>
                            </li>
                            
                            
                        </ul>
                    </li>
                    <li class="nav-item" >
                        <a href="javascript:void(0)" id="btnlogout" class="nav-link">
                        <i class="fas fa-sign-out-alt"></i>
                        <p>Cerrar Sesion</p>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </aside>