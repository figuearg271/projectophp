<footer class="main-footer">
    <strong>Copyright &copy; <?php echo date("Y") ?></strong>
    Todos los derechos reservados.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> <?php echo _APP_VERSION_; ?>
    </div>
  </footer>