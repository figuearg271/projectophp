  
  <?php 
    #Variable para almacenamiento de librerías 
    $librariesCSS=array(
        array('name'=>'Font Awesome', 'route'=>'plugins/fontawesome-free/css/all.min.css'),
        array('name'=>'Tempusdominus Bootstrap', 'route'=>'plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css'),
        array('name'=>'iCheck', 'route'=>'plugins/icheck-bootstrap/icheck-bootstrap.min.css'),
        array('name'=>'overlayScrollbars', 'route'=>'plugins/overlayScrollbars/css/OverlayScrollbars.min.css'),
        array('name'=>'Daterange picker', 'route'=>'plugins/daterangepicker/daterangepicker.css'),
        array('name'=>'summernote', 'route'=>'plugins/summernote/summernote-bs4.min.css'),  
        array('name'=>'Toastr', 'route' => 'plugins/toastr/toastr.min.css'),
        array('name'=>'Select2', 'route'=>
            array('plugins/select2/css/select2.css'),
            array('plugins/select2/css/select2-bootstrap4.css')
        ),
        array('name'=>'SweetAlert', 'route'=>
            array('plugins/sweetalert2/sweetalert2.css')
        ),
        array('name'=>'DataTable', 'route'=>
            array('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css'),
            array('plugins/datatables-responsive/css/responsive.bootstrap4.min.css'),
            array('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')
        ),
        array('name'=>'Theme style', 'route'=>'dist/css/adminlte.min.css')
    );
    foreach($librariesCSS as $item){
        if(gettype($item['route'])!=="array"){
            echo "<link rel='stylesheet' href='".$item['route']."'>";
        }else{
            foreach($item['route'] as $val){
                echo "<link rel='stylesheet' href='".$val."'>";
            }
        }
    }
   
  ?>