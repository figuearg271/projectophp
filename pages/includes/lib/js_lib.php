<?php 
    #Variable para almacenamiento de librerías 
    $librariesJS=array(
        array('name'=>'jQuery', 'route'=>'plugins/jquery/jquery.min.js'),
        array('name'=>'jQuery UI 1.11.4', 'route'=>'plugins/jquery-ui/jquery-ui.min.js'),
        array('name'=>'Bootstrap 4', 'route'=>'plugins/bootstrap/js/bootstrap.bundle.min.js'),
        array('name'=>'daterangepicker', 'route'=>
            array('plugins/moment/moment.min.js'),
            array('plugins/daterangepicker/daterangepicker.js')
        ),
        array('name'=>'Tempusdominus Bootstrap 4', 'route'=>'plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js'),
        array('name'=>'Summernote', 'route'=>'plugins/summernote/summernote-bs4.min.js'),
        array('name'=>'Select2', 'route'=>'plugins/select2/js/select2.js'),
        array('name'=>'SweetAlert', 'route'=>
            array('plugins/sweetalert2/sweetalert2.all.js')
        ),
        array('name'=>'Jquery-validation', 'route'=>
            array('plugins/jquery-validation/jquery.validate.min.js'),
            array('plugins/jquery-validation/additional-methods.min.js'),
            array('plugins/jquery-validation/localization/messages_es.min.js'),
        ),
        array('name'=>'overlayScrollbars', 'route'=>'plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js'),
        array('name'=>'InputMask', 'route'=>'plugins/inputmask/jquery.inputmask.min.js'),
        array('name'=>'Toastr', 'route'=>'plugins/toastr/toastr.min.js'),
        array('name'=>'AdminLTE App', 'route'=>
            array('dist/js/adminlte.js'),
            array('dist/js/demo.js'),
            array('dist/js/pages/dashboard.js')
        )
    );
    foreach($librariesJS as $item){
        if(gettype($item['route'])!=="array"){
            echo "<script type='text/javascript' src='".$item['route']."'></script>";
        }else{
            foreach($item['route'] as $val){
                echo "<script type='text/javascript' src='".$val."'></script>";
            }
        }
    }
   
?>