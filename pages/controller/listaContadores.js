$(document).ready(function() {
    $('#actcontadores').DataTable( { 
      ajax: {
          url: 'appData/data/contadores/actlist.php',
          dataSrc: 'object'
      },
      columns: [
          { data: 'id_contador' },
          { data: 'puesto_en_servicio' },
          { data: 'medida' },
          { data: 'realizada' },
          { data: 'nombre' },
          { 
            data: 'estado',
            render: function (value, type, row, meta ){
                let html = '';
                  if(row.estado == '1'){
                   html = '<span class="text-success"><b>ACTIVO</b></span>';
                    } 
                  else{
                   html = '<span class="text-danger"><b>INACTIVO</b></span>';
              
                  }
                return html; 
                  
            }
         },
         {
            data: null,
             render: function (value, type, row, meta ){
                 let html = '';
                   if(row.estado == '1'){
                    html = '<button type="button" class="btn btn-block btn-danger btn-flat" onclick="activarContadores('+row.id_contador+',0);">Anular</button>';
                     } 
                   else{
                    html = '<button type="button" class="btn btn-block btn-success btn-flat" onclick="activarContadores('+row.id_contador+',1);">Activar</button>';
               
                   }
                 return html; 
                   
             }
        }
      ]} );

      $('#contadores').DataTable( { 
        ajax: {
            url: 'appData/data/contadores/list.php',
            dataSrc: 'object'
        },
        columns: [
            { data: 'id_contador' },
            { data: 'puesto_en_servicio' },
            { data: 'medida' },
            { data: 'realizada' },
            { data: 'nombre' },
            { 
                data: 'estado',
                render: function (value, type, row, meta ){
                    let html = '';
                      if(row.estado == '1'){
                       html = '<span class="text-success"><b>ACTIVO</b></span>';
                        } 
                      else{
                       html = '<span class="text-danger"><b>INACTIVO</b></span>';
                  
                      }
                    return html; 
                      
                }
            },
        ]} );
  });

  function activarContadores(id,estado){
    swal.fire({ 
        icon: 'question', 
        title: '¿Esta seguro de activar este contador ?',
        showCancelButton: true, 
        confirmButtonText: 'Si' 
        }).then(function(result) { 
        if (result.isConfirmed) { 
                $.ajax({
                    url: 'appData/data/contadores/activar.php',
                    type: 'POST',
                    dataType: 'json',
                    data: { id_contador: id, estado: estado}
                }).done(function(response){
                    if(response.status=="success"){ 
                        location.reload(); 
                    }else{
                        Swal.fire({
                            icon: 'warning',
                            title: 'Alerta',
                            html: response.error,
                            "z-index": 10000
                        });
                    }
                }).fail(function(){
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        html: 'Ocurrió un error al realizar la petición',
                        "z-index": 10000
                    });
                })
            } 
            else if (result.isDismissed) { 
                swal.fire( 
                    'Operación cancelada', '', 
                    'warning' 
                ) 
            } 
        });
}