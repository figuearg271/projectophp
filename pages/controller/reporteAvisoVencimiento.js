$(document).ready(function(){
    $.ajax({
        url: 'appData/data/recibo_agua/pdfVencidos.php',
        type: 'POST',
        dataType: 'json'
    }).done(function(response){ 
        if(response.status=="success"){ 
            $("#reporteI").height("700px");
             $("#reporteI").html(response.html); 
        }else{
            Swal.fire({
                icon: 'warning',
                title: 'Alerta',
                html: response.error,
                "z-index": 10000
            });
        }
    }).fail(function(){
        Swal.fire({
            icon: 'error',
            title: 'Error',
            html: 'Ocurrió un error al realizar la petición',
            "z-index": 10000
        });
    })
})