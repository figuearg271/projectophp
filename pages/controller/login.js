$(document).ready(function(){
  
    $("#btnLogin").click(function(){
        $("#frmLogin").validate({
            rules: {
                usuario: {
                    required: true
                },
                password: {
                    required: true
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            },
            submitHandler: function(form){
                var formData=$(form).serialize();
                $.ajax({
                    url: 'appData/data/usuario/login.php',
                    type: 'POST',
                    dataType: 'json',
                    data: formData
                }).done(function(response){
                    if(response.status=="success"){
                        Swal.fire({
                            icon: 'success',
                            title: 'Inicio de sesión',
                            html: 'Bienvenido!',
                            "z-index": 10000
                        }).then((result)=>{
                            location.reload();
                        });
                    }else{
                        Swal.fire({
                            icon: 'warning',
                            title: 'Alerta',
                            html: response.error,
                            "z-index": 10000
                        });
                    }
                }).fail(function(){
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        html: 'Ocurrió un error al realizar la petición',
                        "z-index": 10000
                    });
                })
            }
        })
    })
})