$(document).ready(function() {

    $("#btnModalMovimiento").click(function(){
        $("#frmMovimiento")[0].reset();
        $("#contribuyenteSelect").val(null).trigger("change");
        $("#nuevoMovimientoModal").modal("show");
    });

    $("#subtotalPago, #interesPago").keyup(function(){
        var subtotal = parseFloat($("#subtotalPago").val());
        var interes= parseFloat($("#interesPago").val());
        var total = subtotal - (subtotal * (interes/10));

        $("#totalPago").val(total);

    })

    $("#btnGuardarMovimiento").click(function(){
        $("#frmMovimiento").validate({
            ignore: "",
            rules: {
                id_contribuyente: {
                    required: true
                },
                fechaEmitido: {
                    required: true
                },
                interes_mensual: {
                    required: true,
                    number: true,
                    min: 0,
                    max: 100
                },
                subtotal: {
                    required: true,
                    number: true,
                    min: 0,
                },
                total: {
                    required: true,
                    number: true,
                    min: 0,
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            },
            submitHandler: function(form){
                var formData=$(form).serialize();
                $.ajax({
                    url: 'appData/data/recibo_agua/save.php',
                    type: 'POST',
                    dataType: 'json',
                    data: formData
                }).done(function(response){
                    if(response.status=="success"){
                        Swal.fire({
                            icon: 'success',
                            title: 'Ingreso de recibo de agua',
                            html: 'Registro exitoso',
                            "z-index": 10000
                        }).then((result)=>{
                            location.reload();
                        });
                    }else{
                        Swal.fire({
                            icon: 'warning',
                            title: 'Alerta',
                            html: response.error,
                            "z-index": 10000
                        });
                    }
                }).fail(function(){
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        html: 'Ocurrió un error al realizar la petición',
                        "z-index": 10000
                    });
                })
            }
        })
    });

    $.ajax({
        url: 'appData/data/contribuyente/selectList.php',
        type: 'POST',
        dataType: 'json',
   }).done(function(response){
        if(response.status=="success"){
            $('#contribuyenteSelect').select2({ data: response.object, dropdownAutoWidth: true, allowClear: true, dropdownParent: '#nuevoMovimientoModal', placeholder: 'Seleccione un contribuyente' });

            $("#contribuyenteSelect").val(null).trigger("change");
        }
   });

    $('#movimientos').DataTable( { 
        ajax: {
            url: 'appData/data/ingresos/listAll.php',
            dataSrc: 'object'
        },
        columns: [
            { data: 'id_recibo_agua' }, 
            { data: 'nombre' }, 
            { 
                data: 'subtotal',
                render: function (value, type, row, meta ){
                    let html = '$'+value; 
                    return html; 
                      
                } 
            },
            { 
                data: 'total',
                render: function (value, type, row, meta ){
                    let html = '$'+value; 
                    return html; 
                      
                } 
            },
            { 
                data: 'interes_mensual',
                render: function (value, type, row, meta ){
                    let html = '$'+value; 
                    return html; 
                      
                } 
            },  
            { data: 'fechaEmitido' },
            { data: 'fechaCancelado' },
            { data: 'fechaVencimiento' }, 
            { 
                data: 'estado',
                render: function (value, type, row, meta ){
                    let html = '';
                      if(row.estado == '1'){
                       html = '<span class="text-success"><b>ACTIVO</b></span>';
                        } 
                      else{
                       html = '<span class="text-danger"><b>INACTIVO</b></span>';
                  
                      }
                    return html; 
                      
                }
            } 
            
        ]
    });
});
/*
function activarRecibo(id,estado){
    $.ajax({
        url: 'appData/data/ingresos/activar.php',
        type: 'POST',
        dataType: 'json',
        data: { id_recibo_agua: id, estado: estado}
    }).done(function(response){
        if(response.status=="success"){ 
             location.reload(); 
        }else{
            Swal.fire({
                icon: 'warning',
                title: 'Alerta',
                html: response.error,
                "z-index": 10000
            });
        }
    }).fail(function(){
        Swal.fire({
            icon: 'error',
            title: 'Error',
            html: 'Ocurrió un error al realizar la petición',
            "z-index": 10000
        });
    })
}*/