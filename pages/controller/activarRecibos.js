$(document).ready(function() {
    $('#recibos').DataTable( { 
        ajax: {
            url: 'appData/data/ingresos/list.php',
            dataSrc: 'object'
        },
        columns: [
            { data: 'nombre' },  
            { data: 'fechaEmitido' },
            { data: 'fechaCancelado' },
            { data: 'fechaVencimiento' },
            { 
                data: 'total',
                render: function (value){
                    let html = '$'+value; 
                    return html; 
                      
                } 
            }, 
            { 
                data: 'estado',
                render: function (value, type, row, meta ){
                    let html = '';
                      if(row.estado == '1'){
                       html = '<span class="text-success"><b>ACTIVO</b></span>';
                        } 
                      else{
                       html = '<span class="text-danger"><b>INACTIVO</b></span>';
                  
                      }
                    return html; 
                      
                }
            },
            {
                data: null,
                 render: function (value, type, row, meta ){
                     let html = '';
                       if(row.estado == '1'){
                        html = '<button type="button" class="btn btn-block btn-danger btn-flat" onclick="activarRecibo('+row.id_recibo_agua+',0);">Anular</button>';
                         } 
                       else{
                        html = '<button type="button" class="btn btn-block btn-success btn-flat" onclick="activarRecibo('+row.id_recibo_agua+',1);">Activar</button>';
                   
                       }
                     return html; 
                       
                 }
            }
        ]
    });
});

function activarRecibo(id,estado){
swal.fire({ 
    icon: 'question', 
    title: '¿Esta seguro de activar este recibo ?',
    showCancelButton: true, 
    confirmButtonText: 'Si' 
    }).then(function(result) { 
    if (result.isConfirmed) { 
        $.ajax({
            url: 'appData/data/ingresos/activar.php',
            type: 'POST',
            dataType: 'json',
            data: { id_recibo_agua: id, estado: estado}
        }).done(function(response){
            if(response.status=="success"){ 
                location.reload(); 
            }else{
                Swal.fire({
                    icon: 'warning',
                    title: 'Alerta',
                    html: response.error,
                    "z-index": 10000
                });
            }
        }).fail(function(){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                html: 'Ocurrió un error al realizar la petición',
                "z-index": 10000
            });
        })
        } 
        else if (result.isDismissed) { 
            swal.fire( 
                'Operación cancelada', '', 
                'warning' 
            ) 
        } 
    });
}