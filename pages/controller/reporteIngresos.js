$(document).ready(function() {
    $('#finicio').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    $('#ffin').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' }) 

});

function getReporte(){
    let finicio = $("#finicio").val();
    let ffin = $("#ffin").val();
 
    $.ajax({
        url: 'appData/data/recibo_agua/pdf.php',
        type: 'POST',
        dataType: 'json',
        data: { fecha_inicio: finicio, fecha_fin: ffin}
    }).done(function(response){ 
        if(response.status=="success"){ 
            $("#reporteI").height("700px");
           // console.log(response.object);
             $("#reporteI").html(response.html); 
        }else{
            Swal.fire({
                icon: 'warning',
                title: 'Alerta',
                html: response.error,
                "z-index": 10000
            });
        }
    }).fail(function(){
        Swal.fire({
            icon: 'error',
            title: 'Error',
            html: 'Ocurrió un error al realizar la petición',
            "z-index": 10000
        });
    })
}