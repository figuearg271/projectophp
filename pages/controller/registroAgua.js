$(document).ready(function() {

    $("#btnGuardarMedida").click(function(){
        $("#frmMedida").validate({
            ignore: "",
            rules: {
                medida: {
                    required: true
                },
                id_contador: {
                    required: true
                },
                realizada: {
                    required: true,

                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            },
            submitHandler: function(form){
                var formData=$(form).serialize();
                $.ajax({
                    url: 'appData/data/medidas/save.php',
                    type: 'POST',
                    dataType: 'json',
                    data: formData
                }).done(function(response){
                    if(response.status=="success"){
                        Swal.fire({
                            icon: 'success',
                            title: 'Ingreso de medida',
                            html: 'Registro exitoso',
                            "z-index": 10000
                        }).then((result)=>{
                            location.reload();
                        });
                    }else{
                        Swal.fire({
                            icon: 'warning',
                            title: 'Alerta',
                            html: response.error,
                            "z-index": 10000
                        });
                    }
                }).fail(function(){
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        html: 'Ocurrió un error al realizar la petición',
                        "z-index": 10000
                    });
                })
            }
        })
    });

    $("#btnModalMedida").click(function(){
        $("#frmMedida")[0].reset();
        $("#contadorSelect").val(null).trigger("change");
        $("#nuevaMedidaModal").modal("show");
    });

    $.ajax({
        url: 'appData/data/contadores/selectList.php',
        type: 'POST',
        dataType: 'json',
   }).done(function(response){
        if(response.status=="success"){
            $('#contadorSelect').select2({ data: response.object, dropdownAutoWidth: true, allowClear: true, dropdownParent: '#nuevaMedidaModal', placeholder: 'Seleccione un contribuyente' });

            $("#contadorSelect").val(null).trigger("change");
        }
   });

    $('#ragua').DataTable( { 
        responsive: true,  
        ajax: {
            url: 'appData/data/medidas/list.php',
            dataSrc: 'object'
        },
        columns: [
            { data: 'id_contador' }, 
            { data: 'nombre' }, 
            { data: 'medida' },
            { data: 'realizada' }, 
            { 
                data: 'estado',
                render: function (value, type, row, meta ){
                    let html = '';
                      if(row.estado == '1'){
                       html = '<span class="text-success"><b>ACTIVO</b></span>';
                        } 
                      else{
                       html = '<span class="text-danger"><b>INACTIVO</b></span>';
                  
                      }
                    return html; 
                      
                }
            },
        ]
    });
  });

  function saveMedida(){
    swal.fire({ 
        icon: 'question', 
        title: '¿Esta seguro de ingresar esta medida ?',
        showCancelButton: true, 
        confirmButtonText: 'Si' 
        }).then(function(result) { 
        if (result.isConfirmed) { 
                $.ajax({
                    url: 'appData/data/medidas/save.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {  }
                }).done(function(response){
                    if(response.status=="success"){ 
                        location.reload(); 
                    }else{
                        Swal.fire({
                            icon: 'warning',
                            title: 'Alerta',
                            html: response.error,
                            "z-index": 10000
                        });
                    }
                }).fail(function(){
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        html: 'Ocurrió un error al realizar la petición',
                        "z-index": 10000
                    });
                })
            } 
            else if (result.isDismissed) { 
                swal.fire( 
                    'Operación cancelada', '', 
                    'warning' 
                ) 
            } 
        });
}