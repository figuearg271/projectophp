$(document).ready(function(){

    $("#btnlogout").click(function(){
        swal.fire({ 
            icon: 'question', 
            title: '¿Esta seguro de cerrar sesion ?',
            showCancelButton: true, 
            confirmButtonText: 'Si' 
            }).then(function(result) { 
            if (result.isConfirmed) { 
                    $.ajax({
                        url: 'appData/data/usuario/logout.php',
                        type: 'POST',
                        dataType: 'json' 
                    }).done(function(response){
                        if(response.status=="success"){ 
                            location.reload(); 
                        }else{
                            Swal.fire({
                                icon: 'warning',
                                title: 'Alerta',
                                html: response.error,
                                "z-index": 10000
                            });
                        }
                    }).fail(function(){
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            html: 'Ocurrió un error al realizar la petición',
                            "z-index": 10000
                        });
                    })
                } 
                else if (result.isDismissed) { 
                    swal.fire( 
                        'Operación cancelada', '', 
                        'warning' 
                    ) 
                } 
    });
    
})
})