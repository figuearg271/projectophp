$(document).ready(function(){
    getAvisos();
    function getAvisos(){
        $.ajax({
                url: 'appData/data/avisos/avisos.php',
                type: 'POST',
                dataType: 'json' 
        }).done(function(response){
            if(response.status=="success"){ 
                toastr.error('Hay recibos pendientes de pago.')
            }else{
                Swal.fire({
                    icon: 'warning',
                    title: 'Alerta',
                    html: response.error,
                    "z-index": 10000
                });
            }
        }).fail(function(){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                html: 'Ocurrió un error al realizar la petición',
                "z-index": 10000
            });
        })
    }  
})